
"use strick";

/*  ## Теоретические вопросы
1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
Это взаимодействие между HTML-кодом и JS кодом который позволяет влиять на действия пользователя на странице
2. Какая разница между свойствами HTML-элементов innerHTML и innerText?
innerHTML возвращает все дочерние элементы, а innerText возвращает текстовое содержимое дочерних элементов
3. Как можно обратится к элементу страницы с помощью JS? Какой способ предпочтительнее?
обратится к элементу страницы с помощью JS можно через getElementBy(ID/ Name/ ClassName/ TagName) и querySelector(ALL). Предпочтительней querySelector

## Задания 

Код для заданий лежит в папке project. */

 // 1) Найти все параграфы на странице и установить цвет фона #ff0000

const element = document.querySelectorAll('p')
for (const elementP of element) {
    elementP.style.background = '#ff0000'
}


// 2) Найти элемент с id="optionsList". Вывести в консоль. Найти родительский элемент и вывести в консоль. Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод.

const elementID = document.getElementById("optionsList")
console.log(elementID);
let parentElement = elementID.closest('div')
console.log(parentElement);
let chiledNodes = elementID.childNodes

for (const iterator of chiledNodes) {
    console.log(iterator, 'typeof is ' + typeof iterator);
}


// 3) Установите в качестве контента элемента с классом testParagraph следующий параграф <p>This is a paragraph</p>

const paragraph = document.getElementById("testParagraph")
paragraph.innerHTML='<p>This is a paragraph</p>'
console.log(paragraph);

// 4) Получить элементы <li>, вложенные в элемент с классом main-header и вывеcти их в консоль. Каждому из элементов присвоить новый класс nav-item.

let elementLi = document.querySelectorAll('.main-header>div>ul>li')
for (const iterator of elementLi) {
    iterator.classList.add('nav-item')
    console.log(iterator);
} 


// 5) Найти все элементы с классом section-title. Удалить этот класс у элементов.   

let elementClass = document.querySelectorAll('.section-title')

for (const iterator of elementClass) {
    iterator.remove()
}

